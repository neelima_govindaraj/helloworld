### BTC PROFILE API

### Release Notes
All details related to the release of this API are maintained on wiki and can be accessed using the URL https://wiki.intdigital.ee.co.uk/x/gR5xBQ

### Project Setup
This project uses SVN for code version control. In order to check-out the repository locally you can use:

    svn co https://repository.eead.eeint.co.uk:19443/svn/api/btc-profile-api-v1/

### Structure
Repository contains BTC PROFILE API proxy code.

### Deployment
Each API proxy uses maven to deploy. To deploy the API proxy code to Apigee, run the deploy script using the below command:

    ./deploy.sh {env}

### Testing
Each API proxy has /tests directory with corresponding cucumber test cases...
