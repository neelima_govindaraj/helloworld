Feature:
    #TODO : The feature file to test the resource getParentalControls which will get all parentalControl information

    @Test
  Scenario: Error when the Tracking Header is missing
     Then I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
     And I set Authorization header to Bearer tgG8WIlhBfKIbS7Ecg8sraWUFW6HU4DVu4TTyNqYiNP2hCamTa2RfP
     When I GET /appsmybt/secure/v1/acckey/74bf01a48e244627bc6d976425111cb5/parentalcontrols
     Then response code should be 400
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing header: APIGW-Tracking-Header
      And response body path $.code should be 25
	  
	  
	   @Test
  Scenario: Error when the APIGW-Client-Id is missing
     Then I set APIGW-Tracking-Header header to 1234567
     And I set Authorization header to Bearer tgG8WIlhBfKIbS7Ecg8sraWUFW6HU4DVu4TTyNqYiNP2hCamTa2RfP
     When I GET /appsmybt/secure/v1/acckey/74bf01a48e244627bc6d976425111cb5/parentalcontrols
     Then response code should be 401
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing credentials
      And response body path $.code should be 40

       @Test
  Scenario: Error when the Authorization is missing
     Then I set APIGW-Tracking-Header header to 1234567
      And I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
     When I GET /appsmybt/secure/v1/acckey/74bf01a48e244627bc6d976425111cb5/parentalcontrols
     Then response code should be 401
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing credentials
      And response body path $.code should be 40
	  
	  	  
	   @Test
  Scenario: Success response for parental controls details API
     Then I set APIGW-Tracking-Header header to nikdua07@gmail.com
     And I set Authorization header to Bearer tgG8WIlhBfKIbS7Ecg8sraWUFW6HU4DVu4TTyNqYiNP2hCamTa2RfP
	 And I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
    Given I request spgmocker to return http 200 with file getParentalControls.json for path /acckey/74bf01a48e244627bc6d976425111cb5/parentalcontrols and operation getParentalControlDetails
     Then spgmocker response code should be 200
     When I GET /appsmybt/secure/v1/acckey/74bf01a48e244627bc6d976425111cb5/parentalcontrols
     Then response code should be 200
      And response header Content-Type should be application/hal\+json
      And response body should be valid json
	  
     
