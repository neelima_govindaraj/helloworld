Feature:
    #TODO : The feature file to test the resource getProfile which will get all asset details which is present under single BTONEID.

    @Test
  Scenario: Error when the Tracking Header is missing
     Then I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
     When I GET /appsmybt/secure/v1/package
     Then response code should be 400
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing header: APIGW-Tracking-Header
      And response body path $.code should be 25
	  
	   @Test
  Scenario: Error when the APIGW-Client-Id is missing
     Then I set APIGW-Tracking-Header header to 1234567
     When I GET /appsmybt/secure/v1/package
     Then response code should be 401
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing credentials
      And response body path $.code should be 40

	  	   @Test
  Scenario: Error when the Authorization is missing
     Then I set APIGW-Tracking-Header header to 1234567
      And I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
     When I GET /appsmybt/secure/v1/package
     Then response code should be 401
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing credentials
      And response body path $.code should be 40

   @Test
  Scenario: Success response when N number of BACS are present with multiple products
     Then I set APIGW-Tracking-Header header to nikdua07@gmail.com
     And I set Authorization header to Bearer tgG8WIlhBfKIbS7Ecg8sraWUFW6HU4DVu4TTyNqYiNP2hCamTa2RfP
	 And I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
     Given I request spgmocker to return http 200 with file getProfileWithNBacs.json for path /package and operation getProfileDetails
     Then spgmocker response code should be 200
     When I GET /appsmybt/secure/v1/package
     Then response code should be 200
      And response header Content-Type should be application/hal\+json
      And response body should be valid json
	  
	  
   
