Feature:
    #TODO : The feature file to test the resource getAssets which will individual asset information

   @Test
  Scenario: Error when the Tracking Header is missing
     Then I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
     When I GET /appsmybt/secure/v1/acckey/11007887/product/landline
     Then response code should be 400
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing header: APIGW-Tracking-Header
      And response body path $.code should be 25
	  
	  
	  @Test
  Scenario: Error when the APIGW-Client-Id is missing
     Then I set APIGW-Tracking-Header header to 1234567
     And I set Authorization header to Bearer tgG8WIlhBfKIbS7Ecg8sraWUFW6HU4DVu4TTyNqYiNP2hCamTa2RfP
     When I GET /appsmybt/secure/v1/acckey/11007887/product/landline
     Then response code should be 401
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing credentials
      And response body path $.code should be 40
	  

	  @Test
  Scenario: Error when the Authorization is not for the given resource
     Then I set APIGW-Tracking-Header header to 1234568
	 And I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
     When I GET /appsmybt/secure/v1/acckey/11007887/product/landline
     Then response code should be 401
      And response header Content-Type should be application/hal\+json
      And response body path $.message should be Missing credentials
      And response body path $.code should be 40
	  
	  
	   @Test
  Scenario: Success response for landline product
     Then I set APIGW-Tracking-Header header to nikdua07@gmail.com
	 And I set APIGW-Client-Id header to 7acc9498-3992-4d43-879d-f1012cdcab06
    And I set Authorization header to Bearer tgG8WIlhBfKIbS7Ecg8sraWUFW6HU4DVu4TTyNqYiNP2hCamTa2RfP
    Given I request spgmocker to return http 200 with file landline.json for path /acckey/11007887/product/landline and operation getAssets
     Then spgmocker response code should be 200
     When I GET /appsmybt/secure/v1/acckey/11007887/product/landline
     Then response code should be 200
      And response header Content-Type should be application/hal\+json
      And response body should be valid json
	  
     
