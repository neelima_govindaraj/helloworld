/* jslint node: true */
'use strict';

var apickli = require('apickli');
var Browser = require('node-horseman');
var fs = require("fs");

var reqResource = '';
var locationBasePath = '';
var locationQueryParams = {};
var decodedToken = {}
var apiUrl = "/appsmybt/secure/v1";

module.exports = function() {
    this.Before(function(scenario, callback) {
    	process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
        var oauthRequest = false;
        var testRequest = false;
        var tags = scenario.getTags();
        if (tags.length > 0) {
            //if we start adding multiple tags then will have to revisit this code
            //although tags seemed to affect everything from there on in rather than
            //just the scenario their listed on
            if (tags[0].getName() === "@OAuth") {
                oauthRequest = true;
            }
            else if (tags[0].getName() === "@Test") {
                testRequest = true;
            }
        }
        if (oauthRequest) {
            this.apickli = new apickli.Apickli('https', 'api-dev.ee.co.uk');
            this.apickli.storeValueInScenarioScope("API_KEY", "7acc9498-3992-4d43-879d-f1012cdcab06");
            //this.apickli.storeValueInScenarioScope("API_SEC", "EYs6CWG1Oa5nZxpJ");
        } else if (testRequest) {
            this.apickli = new apickli.Apickli('https', 'ee-nonprod-test.apigee.net');
            this.spgApickli = new apickli.Apickli('https', 'ee-nonprod-test.apigee.net/v2/spg-mocker');
            this.apickli.storeValueInScenarioScope("API_KEY", "7acc9498-3992-4d43-879d-f1012cdcab06");
            //this.apickli.storeValueInScenarioScope("API_SEC", "EYs6CWG1Oa5nZxpJ");
        } else {
            this.apickli = new apickli.Apickli('http', 'api-dev.ee.co.uk');
            this.apickli.storeValueInScenarioScope("API_KEY", "7acc9498-3992-4d43-879d-f1012cdcab06");
            //this.apickli.storeValueInScenarioScope("API_SEC", "EYs6CWG1Oa5nZxpJ");
            this.spgApickli = new apickli.Apickli('http', 'api-dev.ee.co.uk/v2/spg-mocker');
        }
		callback();
    });

    this.After(function(scenario, done) {
        if(this.browser) {
            this.browser.close();
        }

        done();
    });


    this.Given(/^I request spgmocker to return http (.*) with file (.*) for path (.*) and operation (.*)$/, function (httpRespCode, payloadFile, spgPath, operation, callback) {
        var spgMockPayload = {};
        spgMockPayload.url = spgPath;
        spgMockPayload.proxyPath = apiUrl;
        spgMockPayload.responseCode = httpRespCode;
        spgMockPayload.operation = operation;
        var ap = this.spgApickli;
        
        fs.readFile("./tests/integration.dev/fixtures/" + payloadFile, "utf8", function (err, data) {
            if (err) {
                return callback(err);
            }
            var jsonData = JSON.parse(data);
            spgMockPayload.payload = jsonData.payload;
            if (jsonData.header) {
                spgMockPayload.header = jsonData.header;
            }
            ap.setRequestBody(JSON.stringify(spgMockPayload));
            ap.post("/set", function (error, response) {
                if (error) {
                    return callback(new Error(error));
                }
                return callback();
            });
        });
    });

    this.Then(/^spgmocker response code should be (\d+)$/, function (responseCode, callback) {
        var assertion = this.spgApickli.assertResponseCode(responseCode);

        if (assertion.success) {
            callback();
        } else {
            callback(assertion);
        }
    });

	    this.Given(/^I set body using generated refresh token$/, function(callback) {
        this.apickli.setRequestBody("grant_type=refresh_token&refresh_token=" + this.apickli.getGlobalVariable("refreshToken"));
        callback();
    });

    this.Given(/^I remove Authorization header$/, function(callback) {
        delete this.apickli.headers.Authorization;
        callback();
    });

    this.Then(/^dump response (.*)$/, function(type, callback) {
        if ( type == 'all' || type.indexOf('status') >= 0 )
        {
            console.log('         Status code     = ' + this.apickli.httpResponse.statusCode); 
            console.log('         Status Message  = ' + this.apickli.httpResponse.statusMessage); 
        }
        if ( type == 'all' || type.indexOf('body') >= 0 )
        {
            console.log('         Body            = ' + this.apickli.httpResponse.body); 
        }
        if ( type == 'all' || type.indexOf('headers') >= 0 )
        {
            console.log('         Headers : ');
            for ( var a in this.apickli.httpResponse.headers )
            {
                console.log('            ' + a + ' = ' + this.apickli.httpResponse.headers[a]);
            }
        }
        console.log("      ==============================================================");
        callback();
    });


    this.Given(/^I set bearer token to value of access token$/, function(callback) {
        this.apickli.addRequestHeader('Authorization', 'Bearer ' + this.apickli.getGlobalVariable("access_token"));
        callback();
    });
/* =============================================================================================
    Location response Header
============================================================================================= */
    this.Then(/^I parse response header location$/, function(callback) {
        locationBasePath = '';
        locationQueryParams = {};
        var urlComponents = this.apickli.httpResponse.headers.location.split(/#/);
        if ( urlComponents.length > 0 )
        {
            locationBasePath = urlComponents[0];
            if ( urlComponents.length > 1 )
            {
                var qparams = urlComponents[1].split(/&/);
                qparams.forEach(function(q) {
                    var x = q.split(/=/);
                    var key = x[0];
                    var value = x[1];
                    locationQueryParams[key] = value;
                });
            }
        }
        callback();
    });
    this.Then(/^location response header path should be (.*)$/, function(path, callback) {
        if ( locationBasePath != path )
        {
            callback.fail('Unexpected Location header, incorrect URL base path');
        }
        callback();
    });
    this.Then(/^location response header parameter (.*) should be (.*)$/, function(parameter, expression, callback) {
        var re = new RegExp(expression);
        if ( !re.test(locationQueryParams[parameter]) )
        {
            //console.log('Expect [' + expression + '], actual [' + locationQueryParams[parameter] + ']');
            callback.fail('Unexpected Location header, incorrect or missing ' + parameter);
        }
        callback();
    });
    this.Then(/^I store the value of access token from location response header$/, function(callback) {
        this.apickli.setGlobalVariable("access_token", locationQueryParams.access_token);
        callback();
    });
/* =============================================================================================
    End of Location response Header
============================================================================================= */


/* =============================================================================================
    Check JSON values are of correct data type
============================================================================================= */
    this.Then(/^response body typed field (.*) should be (.*)$/, function(path, value, callback) {
        var jsonBody = JSON.parse(this.apickli.httpResponse.body);
        var jsonValue = addQuotesToString(jsonBody[path]);
        var re = new RegExp('^' + value + '$');
        if ( !re.test(jsonValue) )
        {
            callback.fail('Unexpected value for ' + path);
        }
        callback();
    });
};

    
	
	

function addQuotesToString(s)
{
    if ( typeof s == 'string' )
    {
        return '"' + s + '"';
    }
    else
    {
        return s;
    }
}

function getGuid()
{
    var guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 10 | 0,
                v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(10);
        });
    return guid;
}
