 
// added null for local testing via filesystem test html page
var allowedOrigins = context.getVariable("flow.allowed.origins") || ".*.ee.co.uk|.*.bt.com|.*.adobecqms.net|.*null";
var corsMaxAge = context.getVariable("flow.cors.maxage") || 60000;

var requestOrigin = context.getVariable("flow.origin");
var requestMethod = context.getVariable("flow.acr.method") || context.getVariable("request.verb");
var requestHeaders = context.getVariable("request.header.Access-Control-Request-Headers.values") + "";
context.setVariable("debug.req.headersBefore", requestHeaders);
// [''], to remove the opening and closing []
requestHeaders = requestHeaders.substring(1, requestHeaders.length - 1).trim();


var corsSuccess = true;

var allowedOriginsRegex = new RegExp(allowedOrigins);
if (!allowedOriginsRegex.test(requestOrigin)) {
    corsSuccess = false;
}
if (requestHeaders && requestHeaders.length <= 0) {
    corsSuccess = true;
} else {
    var headersArray = requestHeaders.split(",");
// allowed list of headers (below and any headers starting with x-ee-*)
    var allowedHeaders = [
        "accept", "accept-charset", "accept-encoding", "accept-language", "accept-datetime",
        "authorization", "cache-control", "connection", "content-length", "content-md5", "content-type",
        "cookie", "date", "expect", "from", "host", "origin", "pragma", "referer",
        "x-forwarded-for", "x-forwarded-port", "x-forwarded-proto", "user-agent","userid"];

    for (var a = 0; a < headersArray.length; a++) {
        var check = headersArray[a].trim().toLowerCase();
        if (check.length > 0 && allowedHeaders.indexOf(check) < 0 && !check.startsWith("apigw-")) {
            context.setVariable("debug.cors.after.check" + a, check + " ");
            corsSuccess = false;
        }
    }
}


if (corsSuccess) {
    context.setVariable("message.header.Access-Control-Allow-Credentials", "true");
    context.setVariable("message.header.Access-Control-Allow-Origin", requestOrigin);
    context.setVariable("message.header.Access-Control-Allow-Methods", requestMethod);
    context.setVariable("message.header.Access-Control-Allow-Headers", requestHeaders);
    context.setVariable("message.header.Access-Control-Max-Age", corsMaxAge);
}
/**
Headers in the request:
Origin
Access-Control-Request-Method
Access-Control-Request-Headers

Headers in the response:
Access-Control-Allow-Credentials --> true
Access-Control-Allow-Origin --> Origin
Access-Control-Allow-Methods --> {Access-Control-Request-Method}
Access-Control-Allow-Headers
Access-Control-Max-Age
 */

