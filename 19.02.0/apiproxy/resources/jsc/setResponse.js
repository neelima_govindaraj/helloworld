var response = context.getVariable("response.content");
var statusCode = context.getVariable("response.status.code");

constructResponse();
function constructResponse() {
    try {
        context.setVariable("flow.request.length", context.getVariable("request.content").length);
        context.setVariable("flow.response.length", response.length);
        context.setVariable("flow.cachehit", false);
		 
        response = createLink(response);
        if (statusCode == 200) {
            setResponse(response,"OK","200")
        }
        else if (statusCode == 201) {
			setResponse(response,"Created","201")
        }
        // Technical exceptions from back end are not yet confirmed.
        else if (statusCode == 404) {
            setError("true", "404", "Not Found", "60", "Resource not found");
        } else if (statusCode == 403) {
            setError("true", "403", "50", "Forbidden", "Access denied");
        } else if (statusCode == 500) {
            setError("true", "500", "01", "Internal Server Error", "Internal Error");
        } else {
            setError("true", "503", "Service Unavailable", "05", "The service is temporarily unavailable");
        }
    } catch (e) {
        setError("true", "500", "Internal Server Error", "01", "Internal Error");
    }
}

function setResponse(response,phrase,httpCode){
    var sessionCookie = context.getVariable("setCookie.cookie");
    var sessionId = context.getVariable("response.header.JSESSIONID");
	context.setVariable("response.status.code", httpCode);
	context.setVariable("response.content", response);
	context.setVariable("response.reason.phrase", phrase);
    context.setVariable("response.header.Content-Type", "application/hal+json");
    context.setVariable("response.header.Set-Cookie.1", "SESSION="+sessionCookie);
    context.setVariable("response.header.Set-Cookie.2", "JSESSIONID="+sessionId);
}