var trackingHeader = context.getVariable("request.header.APIGW-Tracking-Header");
var authorization = context.getVariable("request.header.Authorization");
var content = context.getVariable("request.content");
var contentType = context.getVariable("request.header.Content-Type");
var clientId = context.getVariable("request.header.APIGW-Client-Id");

validate();

function validate () {
    try {
        if (trackingHeader == null || trackingHeader == "") {
            return setError("true", "400", "Bad Request", "25", "Missing header: APIGW-Tracking-Header");
        }
        if (content != "" && (contentType != "application/json" || contentType != "application/xml")) {
            return setError("true", "415", "Unsupported Media Type", "86", "Unsupported Media Type");
        }
        if (authorization == null || authorization == "" || clientId == null || clientId == "") {
            return setError("true", "401", "Bad Request", "40", "Missing credentials");
        }else {
            authorization = authorization.split(" ");
            context.setVariable("flow.accesstoken", authorization[1]);
        }
    } catch (e) {
        return setError("true", "400", "Bad Request", "22", "Invalid request");
    }
}
/**
 * Function to set error variables. https should made later point // reference     https?:\/\/[^\/]*g
 */
function createLink (responseObject) {
    var alias = context.getVariable("virtualhost.aliases");
    // if(context.getVariable("environment.name")== "test"){
    //     var newUrl = "http://"+alias[0].split(":")[0];
    // }else{
    //     // for BF1 env, as https_vhost is set , alias[0]should suffies
       // var newUrl = "http://"+alias[1].split(":")[0];
    var newUrl = "https://" + alias[0].split(":")[0];
    // }
    if(context.getVariable("environment.name") != "prod"){
		//responseObject = responseObject.replace(/https?:\/\/[Mm][Yy][Bb][Tt]\.[^\/]*/g, function (match, $1) { return newUrl; });
		responseObject = responseObject.replace(/https?:\/\/[Mm][Ss][-][Tt][eE][sS][tT][3]\.[^\/]*/g, function (match, $1) { return newUrl; });
	}else{
		responseObject = responseObject.replace(/https?:\/\/[aA][pP][iI][pP]\.[^\/]*/g, function (match, $1) { return newUrl; });
		//responseObject = responseObject.replace(/https?:\/\/[Dd][Yy]\-[lL][Ii][Vv][eE][^\/]*/g, function (match, $1) { return newUrl; });
	}
    return responseObject;
}

function setError (errorStr, statusCode, reasonPhrase, errorCode, errorMessage) {
    context.setVariable("errorStr", errorStr);
    context.setVariable("sc", statusCode);
    context.setVariable("sr", reasonPhrase);
    context.setVariable("exCode", errorCode);
    context.setVariable("exMessage", errorMessage);
}
