/**
 * Fix for the SPG. It converts application/json to text/html, so changing it to Application/json, instead
 */
/* Remove the code and add accept header param as application/json.
var acceptValues = context.getVariable("request.header.accept.values") + "";
if (acceptValues) {
    acceptValues = acceptValues.substring(1, acceptValues.length - 1); // to omit the [ and ]
    acceptValues = acceptValues.replace(/application\/json/i, "application/json"); // replace all occurences, if present
    context.setVariable("request.header.accept", acceptValues); // set it back to the header
}
*/
var env = context.getVariable("environment.name")
var reqPayload = context.getVariable("request.content")
var pathSuffix = context.getVariable("proxy.pathsuffix")
// setting ths customFlowName
var packages = /\/package$/i;
var products = /\/acckey\/.+\/product\/[a-zA-Z]+$/;
var security = /\/acckey\/.+\/security$/i;
var parentalControls = /\/acckey\/.+\/parentalcontrols$/i;
var vasProductsActivation = /\/acckey\/.+\/vas\/.+$/i;
var pooling = /\/acckey\/.+\/orderref\/.+\/vas\/.+$/i;

if (packages.test(pathSuffix)) {
    context.setVariable("flow.consumer.custom.flow", "getProfileDetails");
} else if (products.test(pathSuffix)) {
    context.setVariable("flow.consumer.custom.flow", "getAssets");
} else if (security.test(pathSuffix)) {
    context.setVariable("flow.consumer.custom.flow", "getSecurityDetails");
} else if (parentalControls.test(pathSuffix)) {
    if (reqPayload !== "") {
        context.setVariable("flow.consumer.custom.flow", "updateFilters");
    } else {
        context.setVariable("flow.consumer.custom.flow", "getParentalControlDetails");
    }
} else if (pooling.test(pathSuffix)) {
    context.setVariable("flow.consumer.custom.flow", "getPooling");
} else if (vasProductsActivation.test(pathSuffix)) {
    var verb = context.getVariable("request.verb")
    if (verb === "POST") {
        context.setVariable("flow.consumer.custom.flow", "activateVasProducts");
    } else {
        if (verb === "DELETE")
            context.setVariable("flow.consumer.custom.flow", "deActivateVasProducts");
    }

} else {

}

if (env == "test") {
    var basePath = context.getVariable("proxy.basepath")
    context.setVariable("request.header.X-EE-DevProxy", basePath);
    context.setVariable("request.header.X-EE-DevOperation", context.getVariable("flow.consumer.custom.flow"));
} else {
    //var oneId = context.getVariable("validate-token.user-id");
    context.setVariable("request.header.accept", "Application/json");
    var content = context.getVariable("request.content")
    if (content != "") {
        context.setVariable("request.header.Content-Type", "Application/json");
    }
   // context.setVariable("request.header.oneId", oneId);// need to set , this value from accesstoken once security gets finalsed 
}