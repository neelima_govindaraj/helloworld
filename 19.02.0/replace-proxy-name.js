/*
Usage: node replace-proxy-name.js profile profile-test
*/

var fs = require('fs');

var from = process.argv[2]
var to = process.argv[3]

fromPath = '/bt-consumer/v1/profile/' + from;
fromApiName = from + '-api';
toPath = '/bt-consumer/v1/profile' + to;
toApiName = to + '-api';

var fromPathRegex = new RegExp(fromPath, 'g');
var fromApiNameRegex = new RegExp(fromApiName, 'g');

var replaceInFile = function(file, regex, to) {
    var data = fs.readFileSync(file, 'utf8');
    var result = data.replace(regex, to);
    fs.writeFileSync(file, result, 'utf8');
};

replaceInFile('./config.json', fromPathRegex, toPath);
replaceInFile('./apiproxy/proxies/default.xml', fromPathRegex, toPath);
replaceInFile('./tests/integration.dev/step_definitions/btc-profile-api.js', fromPathRegex, toPath);
replaceInFile('./pom.xml', fromApiNameRegex, toApiName);
