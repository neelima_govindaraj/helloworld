#!/bin/sh

# we need environment + current directory
environment=$1

# create message to be stored as API Proxy description
commit_rev=$(svn info | grep -m 7 Revision | sed 's/Revision: //')
commit_date=$(svn info | grep -m 7 "Last Changed Date" | sed 's/Last Changed Date: //')
commit_author=$(svn info | grep -m 8 "Last Changed Author" | sed 's/Last Changed Author: //')
commit_branch=$(svn info | grep -m 8 "Relative URL" | sed 's/.*\//branch: /')

commit_message="revision $commit_rev on $commit_date by $commit_author ($commit_branch)"

# replace XML file contents
sed -i "s/<Description>.*<\/Description>/\<Description\>BTC-Profile API : $commit_message\<\/Description\>/" "apiproxy/btc-profile-api-v1.xml"

# install node modules
#if [ "$environment" == "dev" ]; then
#    npm install
#else
	#npm install --only=production
#fi

# fix code styling (only for dev environment)
#if [ "$environment" == "dev" ]; then
 #   node fix-code-style.js
#fi

# Execute maven deployment command
# It assumes that Maven .m2/settings.xml file is used
# to provide username and password
mvn clean apigee-enterprise:deploy -P$environment clean -X